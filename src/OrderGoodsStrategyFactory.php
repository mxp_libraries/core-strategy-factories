<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Common\ValueObject\Tax;
use Maxipost\CoreDomain\Order\ValueObject\Goods\Counting;
use Maxipost\CoreDomain\Order\ValueObject\Goods\ItemId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderGoodsStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    /**
     * @param string $rootClassName
     * @return array
     */
    public static function getConfig(string $rootClassName): array
    {
        return [
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::IS_ARRAY => true,
            FormStrategyBuilder::NESTED_FIELDS => [
                'itemId' => [
                    FormStrategyBuilder::DTO => ItemId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                ],
                'counting' => [
                    FormStrategyBuilder::DTO => Counting::class
                ],
                'tax' => [
                    FormStrategyBuilder::DTO => Tax::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ]
            ]
        ];
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray(self::getConfig($rootClassName));
    }
}
<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Common\ValueObject\Address;
use Maxipost\CoreDomain\Common\ValueObject\Contact\Role;
use Maxipost\CoreDomain\Common\ValueObject\InternalCode;
use Maxipost\CoreDomain\Common\ValueObject\OperatingTime;
use Maxipost\CoreDomain\Common\ValueObject\Time;
use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Warehouse\ValueObject\ApishipId;
use Maxipost\CoreDomain\Warehouse\ValueObject\Contact;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class WarehouseStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => WarehouseId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'warehouseApishipId' => [
                    FormStrategyBuilder::DTO => ApishipId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'value'
                ],
                'counterpartyId' => [
                    FormStrategyBuilder::DTO => CounterpartyId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'address' => [
                    FormStrategyBuilder::DTO => Address::class,
                ],
                'contacts' => [
                    FormStrategyBuilder::DTO => Contact::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'role' => [
                            FormStrategyBuilder::DTO => Role::class,
                            FormStrategyBuilder::SINGLE_NAME => 'id'
                        ]
                    ]
                ],
                'operatingTime' => [
                    FormStrategyBuilder::DTO => OperatingTime::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'openedFrom' => $this->getTime(),
                        'openedTo' => $this->getTime(),
                        'breakFrom' => $this->getTime(),
                        'breakTo' => $this->getTime()
                    ]
                ],
                'internalCode' => [
                    FormStrategyBuilder::DTO => InternalCode::class,
                    FormStrategyBuilder::SINGLE_NAME => 'value'
                ]
            ]
        ]);
    }

    /**
     * @return array
     */
    private function getTime(): array
    {
        return [
            FormStrategyBuilder::DTO => Time::class,
            FormStrategyBuilder::SINGLE_NAME => 'value'
        ];
    }
}
<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\CoreDomain\User\ValueObject\Permissions;
use Maxipost\CoreDomain\User\ValueObject\Phone;
use Maxipost\CoreDomain\User\ValueObject\Role;
use Maxipost\CoreDomain\User\ValueObject\UserId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class UserStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => UserId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                ],
                'roles' => [
                    FormStrategyBuilder::DTO => Role::class,
                    FormStrategyBuilder::SINGLE_NAME => 'value',
                    FormStrategyBuilder::IS_ARRAY => true,
                ],
                'phone' => [
                    FormStrategyBuilder::DTO => Phone::class,
                    FormStrategyBuilder::SINGLE_NAME => 'value',
                ],
                'permissions' => [
                    FormStrategyBuilder::DTO => Permissions::class,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'legalPersonIds' => [
                            FormStrategyBuilder::DTO => LegalPersonId::class,
                            FormStrategyBuilder::IS_ARRAY => true,
                            FormStrategyBuilder::SINGLE_NAME => 'uuid',
                        ],
                        'warehouseIds' => [
                            FormStrategyBuilder::DTO => WarehouseId::class,
                            FormStrategyBuilder::IS_ARRAY => true,
                            FormStrategyBuilder::SINGLE_NAME => 'uuid',
                        ],
                    ]
                ],
            ]
        ]);
    }
}
<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use DateTimeImmutable;
use DateTimeInterface;
use Maxipost\CoreDomain\Common\ValueObject\Date;
use Maxipost\CoreDomain\Courier\ValueObject\CourierId;
use Maxipost\CoreDomain\Courier\ValueObject\Credentials;
use Maxipost\CoreDomain\Courier\ValueObject\Document;
use Maxipost\CoreDomain\Courier\ValueObject\Transport;
use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\CoreDomain\User\ValueObject\Permissions;
use Maxipost\CoreDomain\User\ValueObject\UserId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class CourierStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => CourierId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                ],
                'userId' => [
                    FormStrategyBuilder::DTO => UserId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                ],
                'permissions' => [
                    FormStrategyBuilder::DTO => Permissions::class,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'legalPersonIds' => [
                            FormStrategyBuilder::DTO => LegalPersonId::class,
                            FormStrategyBuilder::IS_ARRAY => true,
                            FormStrategyBuilder::SINGLE_NAME => 'uuid',
                        ],
                        'warehouseIds' => [
                            FormStrategyBuilder::DTO => WarehouseId::class,
                            FormStrategyBuilder::IS_ARRAY => true,
                            FormStrategyBuilder::SINGLE_NAME => 'uuid',
                        ],
                    ]
                ],
                'credentials' => [
                    FormStrategyBuilder::DTO => Credentials::class,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'birthday' => [
                            FormStrategyBuilder::DTO => Date::class,
                            FormStrategyBuilder::SINGLE_NAME => 'value',
                        ],
                    ],
                ],
                'transport' => [
                    FormStrategyBuilder::DTO => Transport::class,
                ],
                'documents' => [
                    FormStrategyBuilder::DTO => Document::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'timestamp' => [
                            FormStrategyBuilder::DTO => DateTimeImmutable::class,
                            FormStrategyBuilder::HYDRATE_CALLBACK => static function ($date, $dto) {
                                /** @var \DateTimeImmutable $dto */
                                return $dto::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $date);
                            },
                            FormStrategyBuilder::EXTRACT_CALLBACK => static function (DateTimeImmutable $data) {
                                return $data->format(DateTimeInterface::RFC3339_EXTENDED);
                            },
                        ],
                    ],
                ],
            ],
        ]);
    }
}
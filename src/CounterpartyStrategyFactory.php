<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Common\ValueObject\Contact\Role;
use Maxipost\CoreDomain\Contract\ValueObject\ContractId;
use Maxipost\CoreDomain\Counterparty\ValueObject\Contact;
use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Counterparty\ValueObject\Integration;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class CounterpartyStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => CounterpartyId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                ],
                'integration' => [
                    FormStrategyBuilder::DTO => Integration::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'providerConnectIds' => [
                            FormStrategyBuilder::DTO => ContractId::class,
                            FormStrategyBuilder::IS_ARRAY => true,
                            FormStrategyBuilder::SINGLE_NAME => 'uuid',
                        ]
                    ]
                ],
                'contacts' => [
                    FormStrategyBuilder::DTO => Contact::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'role' => [
                            FormStrategyBuilder::DTO => Role::class,
                            FormStrategyBuilder::SINGLE_NAME => 'id'
                        ]
                    ]
                ]
            ]
        ]);
    }
}
<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Order\ValueObject\CashOnDelivery\PaymentType;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderPaymentTypeStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => OrderId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'value' => [
                    FormStrategyBuilder::DTO => PaymentType::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'paymentType' => [
                    FormStrategyBuilder::DTO => PaymentType::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ]
            ]
        ]);
    }
}
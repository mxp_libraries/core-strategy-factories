<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\DomainEventSourcing\AggregateRootId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

/**
 * Class DefaultStrategyFactory
 *
 * Default strategy factory provides non-parameterized strategy with root class
 * "_id" has added because it required by default by some reasons
 *
 * @package Maxipost\CoreStrategyFactories
 */
class DefaultStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    /**
     * @param string $rootClassName
     * @return array
     */
    public static function getConfig(string $rootClassName): array
    {
        return [
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => AggregateRootId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ]
            ]
        ];
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray(self::getConfig($rootClassName));
    }
}
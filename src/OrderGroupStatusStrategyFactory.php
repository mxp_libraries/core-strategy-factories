<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;


use Maxipost\CoreDomain\OrderGroup\ValueObject\Status;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderGroupStatusStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray(
            [
                FormStrategyBuilder::DTO => $rootClassName,
                FormStrategyBuilder::NESTED_FIELDS => [
                    'status' => [
                        FormStrategyBuilder::DTO => Status::class,
                        FormStrategyBuilder::SINGLE_NAME => 'id'
                    ],
                ]
            ]
        );
    }
}
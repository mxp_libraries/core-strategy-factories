<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;


use Maxipost\CoreDomain\Contract\ValueObject\ServiceCategory;
use Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject\ExternalContractor;
use Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject\OrderStatusConstructorModelId;
use Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject\Stage;
use Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject\Status;
use Maxipost\CoreDomain\OrderStatusConstructorModel\ValueObject\SubStatus;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderStatusConstructorModelStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => OrderStatusConstructorModelId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'stageId' => [
                    FormStrategyBuilder::DTO => Stage::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id',
                ],
                'serviceCategoryId' => [
                    FormStrategyBuilder::DTO => ServiceCategory::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'statusId' => [
                    FormStrategyBuilder::DTO => Status::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'subStatusId' => [
                    FormStrategyBuilder::DTO => SubStatus::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'externalContractors' => [
                    FormStrategyBuilder::DTO => ExternalContractor::class,
                    FormStrategyBuilder::IS_ARRAY => true
                ]
            ]
        ]);
    }
}

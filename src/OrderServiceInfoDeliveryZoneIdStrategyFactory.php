<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\DeliveryZone\ValueObject\DeliveryZoneId;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderServiceInfoDeliveryZoneIdStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    /**
     * @param string $rootClassName
     * @return array
     */
    public static function getConfig(string $rootClassName): array
    {
        return [
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => OrderId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'deliveryZoneId' => [
                    FormStrategyBuilder::DTO => DeliveryZoneId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'value' => [
                    FormStrategyBuilder::DTO => DeliveryZoneId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
            ]
        ];
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray(self::getConfig($rootClassName));
    }
}

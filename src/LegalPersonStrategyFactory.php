<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\LegalPerson\ValueObject\BankDetails;
use Maxipost\CoreDomain\LegalPerson\ValueObject\Info;
use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class LegalPersonStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
                FormStrategyBuilder::DTO => $rootClassName,
                FormStrategyBuilder::NESTED_FIELDS => [
                    '_id' => [
                        FormStrategyBuilder::DTO => LegalPersonId::class,
                        FormStrategyBuilder::SINGLE_NAME => 'uuid'
                    ],
                    'counterpartyId' => [
                        FormStrategyBuilder::DTO => CounterpartyId::class,
                        FormStrategyBuilder::SINGLE_NAME => 'uuid'
                    ],
                    'info' => [
                        FormStrategyBuilder::DTO => Info::class,
                    ],
                    'bankDetails' => [
                        FormStrategyBuilder::DTO => BankDetails::class,
                        FormStrategyBuilder::IS_ARRAY => true
                    ]
                ]
            ]
        );
    }
}
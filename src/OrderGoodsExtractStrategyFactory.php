<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Order\ValueObject\Goods;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderGoodsExtractStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    /**
     * @param string $rootClassName
     * @return array
     */
    public static function getConfig(string $rootClassName): array
    {
        return [
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => OrderId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'goods' => OrderGoodsStrategyFactory::getConfig(Goods::class)
            ]
        ];
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray(self::getConfig($rootClassName));
    }
}
<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;


use DateTimeImmutable;
use DateTimeInterface;
use Maxipost\CoreDomain\Courier\ValueObject\CourierId;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\CoreDomain\OrderGroup\ValueObject\OrderGroupId;
use Maxipost\CoreDomain\OrderGroup\ValueObject\Status;
use Maxipost\CoreDomain\OrderGroup\ValueObject\Type;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderGroupStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray(
            [
                FormStrategyBuilder::DTO => $rootClassName,
                FormStrategyBuilder::NESTED_FIELDS => [
                    '_id' => [
                        FormStrategyBuilder::DTO => OrderGroupId::class,
                        FormStrategyBuilder::SINGLE_NAME => 'uuid'
                    ],
                    'courierId' => [
                        FormStrategyBuilder::DTO => CourierId::class,
                        FormStrategyBuilder::SINGLE_NAME => 'uuid'
                    ],
                    'status' => [
                        FormStrategyBuilder::DTO => Status::class,
                        FormStrategyBuilder::SINGLE_NAME => 'id'
                    ],
                    'type' => [
                        FormStrategyBuilder::DTO => Type::class,
                        FormStrategyBuilder::SINGLE_NAME => 'id'
                    ],
                    'sentAt' => [
                        FormStrategyBuilder::DTO => DateTimeImmutable::class,
                        FormStrategyBuilder::EXTRACT_CALLBACK => static function (DateTimeImmutable $data) {
                            return $data->format(DateTimeInterface::RFC3339_EXTENDED);
                        },
                        FormStrategyBuilder::HYDRATE_CALLBACK => static function ($data) {
                            if ($data === null) {
                                return null;
                            }
                            return DateTimeImmutable::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $data);
                        },
                        FormStrategyBuilder::IS_NEED_TO_HYDRATE_IF_EMPTY => false
                    ],
                    'receivedAt' => [
                        FormStrategyBuilder::DTO => DateTimeImmutable::class,
                        FormStrategyBuilder::EXTRACT_CALLBACK => static function (DateTimeImmutable $data) {
                            return $data->format(DateTimeInterface::RFC3339_EXTENDED);
                        },
                        FormStrategyBuilder::HYDRATE_CALLBACK => static function ($data) {
                            if ($data === null) {
                                return null;
                            }
                            return DateTimeImmutable::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $data);
                        },
                        FormStrategyBuilder::IS_NEED_TO_HYDRATE_IF_EMPTY => false
                    ],
                    'createdAt' => [
                        FormStrategyBuilder::DTO => DateTimeImmutable::class,
                        FormStrategyBuilder::EXTRACT_CALLBACK => static function (DateTimeImmutable $data) {
                            return $data->format(DateTimeInterface::RFC3339_EXTENDED);
                        },
                        FormStrategyBuilder::HYDRATE_CALLBACK => static function ($data) {
                            if ($data === null) {
                                return null;
                            }
                            return DateTimeImmutable::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $data);
                        },
                        FormStrategyBuilder::IS_NEED_TO_HYDRATE_IF_EMPTY => false
                    ],
                    'fromWarehouseId' => [
                        FormStrategyBuilder::DTO => WarehouseId::class,
                        FormStrategyBuilder::SINGLE_NAME => 'uuid'
                    ],
                    'toWarehouseId' => [
                        FormStrategyBuilder::DTO => WarehouseId::class,
                        FormStrategyBuilder::SINGLE_NAME => 'uuid'
                    ],
                    'orderIds' => [
                        FormStrategyBuilder::DTO => OrderId::class,
                        FormStrategyBuilder::SINGLE_NAME => 'uuid',
                        FormStrategyBuilder::IS_ARRAY => true
                    ]
                ]
            ]
        );
    }
}
<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Common\ValueObject\Address;
use Maxipost\CoreDomain\Common\ValueObject\InternalCode;
use Maxipost\CoreDomain\Common\ValueObject\OperatingTime;
use Maxipost\CoreDomain\Common\ValueObject\Photo;
use Maxipost\CoreDomain\Common\ValueObject\Time;
use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\OrderServicePoint\ValueObject\ApishipId;
use Maxipost\CoreDomain\OrderServicePoint\ValueObject\Contacts;
use Maxipost\CoreDomain\OrderServicePoint\ValueObject\OrderServicePointId;
use Maxipost\CoreDomain\OrderServicePoint\ValueObject\Parameters;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderServicePointStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => OrderServicePointId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                ],
                'orderServicePointApishipId' => [
                    FormStrategyBuilder::DTO => ApishipId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'value',
                ],
                'counterpartyId' => [
                    FormStrategyBuilder::DTO => CounterpartyId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                ],
                'address' => [
                    FormStrategyBuilder::DTO => Address::class,
                ],
                'contacts' => [
                    FormStrategyBuilder::DTO => Contacts::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                ],
                'operatingTime' => [
                    FormStrategyBuilder::DTO => OperatingTime::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'openedFrom' => [
                            FormStrategyBuilder::DTO => Time::class,
                            FormStrategyBuilder::SINGLE_NAME => 'value',
                        ],
                        'openedTo' => [
                            FormStrategyBuilder::DTO => Time::class,
                            FormStrategyBuilder::SINGLE_NAME => 'value',
                        ],
                        'breakFrom' => [
                            FormStrategyBuilder::DTO => Time::class,
                            FormStrategyBuilder::SINGLE_NAME => 'value',
                        ],
                        'breakTo' => [
                            FormStrategyBuilder::DTO => Time::class,
                            FormStrategyBuilder::SINGLE_NAME => 'value',
                        ],
                    ],
                ],
                'parameters' => [
                    FormStrategyBuilder::DTO => Parameters::class,
                ],
                'photos' => [
                    FormStrategyBuilder::DTO => Photo::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                    FormStrategyBuilder::SINGLE_NAME => 'value',
                ],
                'internalCode' => [
                    FormStrategyBuilder::DTO => InternalCode::class,
                    FormStrategyBuilder::SINGLE_NAME => 'value'
                ],
            ],
        ]);
    }
}
<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Common\ValueObject\Tax;
use Maxipost\CoreDomain\Contract\ValueObject\ContractId;
use Maxipost\CoreDomain\Contract\ValueObject\ContractService;
use Maxipost\CoreDomain\Contract\ValueObject\ContractType;
use Maxipost\CoreDomain\Contract\ValueObject\CostReductionFactors;
use Maxipost\CoreDomain\Contract\ValueObject\CounterpartyType;
use Maxipost\CoreDomain\Contract\ValueObject\Delivery;
use Maxipost\CoreDomain\Contract\ValueObject\MoneyTransferType;
use Maxipost\CoreDomain\Contract\ValueObject\NettingTariffType;
use Maxipost\CoreDomain\Contract\ValueObject\NettingType;
use Maxipost\CoreDomain\Contract\ValueObject\ServiceCategory;
use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\CoreDomain\Route\ValueObject\DangerTypeId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class ContractStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => ContractId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'clientLegalPersonId' => [
                    FormStrategyBuilder::DTO => LegalPersonId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'contractorLegalPersonId' => [
                    FormStrategyBuilder::DTO => LegalPersonId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'taxId' => [
                    FormStrategyBuilder::DTO => Tax::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'contractType' => [
                    FormStrategyBuilder::DTO => ContractType::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'counterpartyType' => [
                    FormStrategyBuilder::DTO => CounterpartyType::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'nettingType' => [
                    FormStrategyBuilder::DTO => NettingType::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'nettingTariffType' => [
                    FormStrategyBuilder::DTO => NettingTariffType::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'moneyTransferType' => [
                    FormStrategyBuilder::DTO => MoneyTransferType::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id'
                ],
                'delivery' => [
                    FormStrategyBuilder::DTO => Delivery::class
                ],
                'costReductionFactors' => [
                    FormStrategyBuilder::DTO => CostReductionFactors::class
                ],
                'contractServices' => [
                    FormStrategyBuilder::DTO => ContractService::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'serviceCategoryId' => [
                            FormStrategyBuilder::DTO => ServiceCategory::class,
                            FormStrategyBuilder::SINGLE_NAME => 'id'
                        ]
                    ]
                ],
                'dangerTypes' => [
                    FormStrategyBuilder::DTO => DangerTypeId::class,
                    FormStrategyBuilder::IS_ARRAY => true,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ]
            ]
        ]);
    }
}
<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use DateTimeImmutable;
use DateTimeInterface;
use Maxipost\CoreDomain\Common\ValueObject\Address;
use Maxipost\CoreDomain\Common\ValueObject\Date;
use Maxipost\CoreDomain\Common\ValueObject\DateTimeIntervalSimple;
use Maxipost\CoreDomain\Common\ValueObject\Tax;
use Maxipost\CoreDomain\Contract\ValueObject\ContractId;
use Maxipost\CoreDomain\Courier\ValueObject\CourierId;
use Maxipost\CoreDomain\DeliveryService\ValueObject\DeliveryServiceCode;
use Maxipost\CoreDomain\DeliveryZone\ValueObject\DeliveryZoneId;
use Maxipost\CoreDomain\LegalPerson\ValueObject\LegalPersonId;
use Maxipost\CoreDomain\Order\Status;
use Maxipost\CoreDomain\Order\ValueObject\AdditionalDeliveryService;
use Maxipost\CoreDomain\Order\ValueObject\ApishipId;
use Maxipost\CoreDomain\Order\ValueObject\CashOnDelivery;
use Maxipost\CoreDomain\Order\ValueObject\CashOnDelivery\PaymentType;
use Maxipost\CoreDomain\Order\ValueObject\CashOnDeliveryStatus;
use Maxipost\CoreDomain\Order\ValueObject\CompletedAdditionalDeliveryService;
use Maxipost\CoreDomain\Order\ValueObject\Contracts;
use Maxipost\CoreDomain\Order\ValueObject\Deliverer;
use Maxipost\CoreDomain\Order\ValueObject\DeliveryOrder;
use Maxipost\CoreDomain\Order\ValueObject\Dimensions;
use Maxipost\CoreDomain\Order\ValueObject\Goods;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\CoreDomain\Order\ValueObject\PaymentOnDelivery;
use Maxipost\CoreDomain\Order\ValueObject\PaymentOnPrincipal;
use Maxipost\CoreDomain\Order\ValueObject\PickupOrder;
use Maxipost\CoreDomain\Order\ValueObject\Place;
use Maxipost\CoreDomain\Order\ValueObject\Principal;
use Maxipost\CoreDomain\Order\ValueObject\Recipient;
use Maxipost\CoreDomain\Order\ValueObject\Recipient\Contacts;
use Maxipost\CoreDomain\Order\ValueObject\Sender;
use Maxipost\CoreDomain\Order\ValueObject\ServiceInfo;
use Maxipost\CoreDomain\Order\ValueObject\ServiceType;
use Maxipost\CoreDomain\OrderServicePoint\ValueObject\OrderServicePointId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray(
            [
                FormStrategyBuilder::DTO => $rootClassName,
                FormStrategyBuilder::NESTED_FIELDS => [
                    '_id' => [
                        FormStrategyBuilder::DTO => OrderId::class,
                        FormStrategyBuilder::SINGLE_NAME => 'uuid',
                    ],
                    'orderApishipId' => [
                        FormStrategyBuilder::DTO => ApishipId::class,
                        FormStrategyBuilder::SINGLE_NAME => 'value',
                    ],
                    'serviceInfo' => [
                        FormStrategyBuilder::DTO => ServiceInfo::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'courierId' => [
                                FormStrategyBuilder::DTO => CourierId::class,
                                FormStrategyBuilder::SINGLE_NAME => 'uuid',
                            ],
                            'deliveryZoneId' => [
                                FormStrategyBuilder::DTO => DeliveryZoneId::class,
                                FormStrategyBuilder::SINGLE_NAME => 'uuid'
                            ],
                            'createdAt' => [
                                FormStrategyBuilder::DTO => DateTimeImmutable::class,
                                FormStrategyBuilder::EXTRACT_CALLBACK => static function (DateTimeImmutable $data) {
                                    return $data->format(DateTimeInterface::RFC3339_EXTENDED);
                                },
                                FormStrategyBuilder::HYDRATE_CALLBACK => static function ($data) {
                                    return DateTimeImmutable::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $data);
                                },
                            ],
                        ],
                    ],
                    'statuses' => array_merge(OrderStatusStrategyFactory::getConfig(Status::class), [
                        FormStrategyBuilder::IS_ARRAY => true,
                    ]),
                    'serviceType' => [
                        FormStrategyBuilder::DTO => ServiceType::class,
                        FormStrategyBuilder::SINGLE_NAME => 'id',
                    ],
                    'contracts' => [
                        FormStrategyBuilder::DTO => Contracts::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'senderPrincipalContractId' => $this->getContract(),
                            'principalDelivererContractId' => $this->getContract(),
                        ],
                    ],
                    'principal' => [
                        FormStrategyBuilder::DTO => Principal::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'warehouseId' => $this->getWarehouse(),
                            'legalPersonId' => $this->getLegalPerson(),
                            'contractId' => $this->getContract(),
                        ],
                    ],
                    'deliverer' => [
                        FormStrategyBuilder::DTO => Deliverer::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'warehouseId' => $this->getWarehouse(),
                            'legalPersonId' => $this->getLegalPerson(),
                        ],
                    ],
                    'deliveryOrder' => [
                        FormStrategyBuilder::DTO => DeliveryOrder::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'dateTimeInterval' => OrderDeliveryDateStrategyFactory::getConfig(DateTimeIntervalSimple::class),
                            'orderServicePointId' => [
                                FormStrategyBuilder::DTO => OrderServicePointId::class,
                                FormStrategyBuilder::SINGLE_NAME => 'uuid',
                                FormStrategyBuilder::IS_NEED_TO_HYDRATE_IF_EMPTY => false,
                            ],
                        ],
                    ],
                    'pickupOrder' => [
                        FormStrategyBuilder::DTO => PickupOrder::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'dateTimeInterval' => OrderDeliveryDateStrategyFactory::getConfig(DateTimeIntervalSimple::class),
                            'pickupType' => [
                                FormStrategyBuilder::DTO => PickupOrder\Type::class,
                                FormStrategyBuilder::SINGLE_NAME => 'id',
                            ],
                            'warehouseId' => $this->getWarehouse(),
                        ],
                    ],
                    'sender' => [
                        FormStrategyBuilder::DTO => Sender::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'warehouseId' => $this->getWarehouse(),
                            'legalPersonId' => $this->getLegalPerson(),
                        ]
                    ],
                    'recipient' => [
                        FormStrategyBuilder::DTO => Recipient::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'address' => [
                                FormStrategyBuilder::DTO => Address::class,
                            ],
                            'contacts' => [
                                FormStrategyBuilder::DTO => Contacts::class,
                            ],
                        ],
                    ],
                    'cashOnDelivery' => [
                        FormStrategyBuilder::DTO => CashOnDelivery::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'paymentType' => [
                                FormStrategyBuilder::DTO => PaymentType::class,
                                FormStrategyBuilder::SINGLE_NAME => 'id',
                            ],
                        ],
                    ],
                    'cashOnDeliveryStatus' => [
                        FormStrategyBuilder::DTO => CashOnDeliveryStatus::class,
                        FormStrategyBuilder::SINGLE_NAME => 'id',
                    ],
                    'paymentOnPrincipal' => [FormStrategyBuilder::DTO => PaymentOnPrincipal::class],
                    'paymentOnDelivery' => [FormStrategyBuilder::DTO => PaymentOnDelivery::class],
                    'additionalDeliveryServices' => [
                        FormStrategyBuilder::DTO => AdditionalDeliveryService::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'deliveryServiceId' => [
                                FormStrategyBuilder::DTO => DeliveryServiceCode::class,
                                FormStrategyBuilder::SINGLE_NAME => 'value',
                            ],
                            'tax' => [
                                FormStrategyBuilder::DTO => Tax::class,
                                FormStrategyBuilder::SINGLE_NAME => 'id',
                            ],
                            'servicePayer' => [
                                FormStrategyBuilder::DTO => AdditionalDeliveryService\ServicePayer::class,
                                FormStrategyBuilder::SINGLE_NAME => 'id',
                            ],
                        ],
                        FormStrategyBuilder::IS_ARRAY => true,
                    ],
                    'completedAdditionalDeliveryServices' => [
                        FormStrategyBuilder::DTO => CompletedAdditionalDeliveryService::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'deliveryServiceId' => [
                                FormStrategyBuilder::DTO => DeliveryServiceCode::class,
                                FormStrategyBuilder::SINGLE_NAME => 'value',
                            ],
                            'serviceDate' => [
                                FormStrategyBuilder::DTO => Date::class,
                                FormStrategyBuilder::SINGLE_NAME => 'value',
                            ],
                        ],
                        FormStrategyBuilder::IS_ARRAY => true,
                    ],
                    'dimensions' => OrderDimensionsStrategyFactory::getConfig(Dimensions::class),
                    'places' => [
                        FormStrategyBuilder::DTO => Place::class,
                        FormStrategyBuilder::NESTED_FIELDS => [
                            'dimensions' => [
                                FormStrategyBuilder::DTO => Dimensions::class,
                            ],
                        ],
                        FormStrategyBuilder::IS_ARRAY => true,
                    ],
                    'goods' => OrderGoodsStrategyFactory::getConfig(Goods::class),
                ],
            ]
        );
    }

    /**
     * @return array
     */
    private function getWarehouse(): array
    {
        return [
            FormStrategyBuilder::DTO => WarehouseId::class,
            FormStrategyBuilder::SINGLE_NAME => 'uuid',
        ];
    }

    /**
     * @return array
     */
    private function getLegalPerson(): array
    {
        return [
            FormStrategyBuilder::DTO => LegalPersonId::class,
            FormStrategyBuilder::SINGLE_NAME => 'uuid',
        ];
    }

    /**
     * @return array
     */
    private function getContract(): array
    {
        return [
            FormStrategyBuilder::DTO => ContractId::class,
            FormStrategyBuilder::SINGLE_NAME => 'uuid',
        ];
    }
}

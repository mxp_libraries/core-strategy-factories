<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Counterparty\ValueObject\CounterpartyId;
use Maxipost\CoreDomain\Route\ValueObject\CurrencyId;
use Maxipost\CoreDomain\Route\ValueObject\DangerSubtypeId;
use Maxipost\CoreDomain\Route\ValueObject\DangerTypeId;
use Maxipost\CoreDomain\Route\ValueObject\RouteId;
use Maxipost\CoreDomain\Route\ValueObject\Schedule;
use Maxipost\CoreDomain\Route\ValueObject\TransportTypeId;
use Maxipost\CoreDomain\Warehouse\ValueObject\WarehouseId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class RouteStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray([
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => RouteId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'counterpartyId' => [
                    FormStrategyBuilder::DTO => CounterpartyId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'fromWarehouseId' => [
                    FormStrategyBuilder::DTO => WarehouseId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'toWarehouseId' => [
                    FormStrategyBuilder::DTO => WarehouseId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'transportTypeId' => [
                    FormStrategyBuilder::DTO => TransportTypeId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',

                ],
                'dangerTypeId' => [
                    FormStrategyBuilder::DTO => DangerTypeId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                    FormStrategyBuilder::IS_ARRAY => true,


                ],
                'dangerSubtypeId' => [
                    FormStrategyBuilder::DTO => DangerSubtypeId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                    FormStrategyBuilder::IS_ARRAY => true,


                ],
                'currencyId' => [
                    FormStrategyBuilder::DTO => CurrencyId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],

                'schedule' => [
                    FormStrategyBuilder::DTO => Schedule::class,
                    FormStrategyBuilder::IS_ARRAY => true,

                ],
            ]
        ]);
    }
}
<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;


use DateTimeImmutable;
use DateTimeInterface;
use Maxipost\CoreDomain\Order\ValueObject\CashOnDelivery\PaymentType;
use Maxipost\CoreDomain\Order\ValueObject\OrderStatusId;
use Maxipost\CoreDomain\Order\ValueObject\Status\Goods;
use Maxipost\CoreDomain\OrderStatusModel\ValueObject\OrderStatusModelId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderStatusStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    /**
     * @param string $rootClassName
     * @return array
     */
    public static function getConfig(string $rootClassName): array
    {
        return [
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => OrderStatusId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid',
                ],
                'paymentType' => [
                    FormStrategyBuilder::DTO => PaymentType::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id',
                ],
                'sourceCreatedAt' => [
                    FormStrategyBuilder::DTO => DateTimeImmutable::class,
                    FormStrategyBuilder::HYDRATE_CALLBACK => static function ($date, $dto) {
                        /** @var \DateTimeImmutable $dto */
                        return $dto::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $date);
                    },
                    FormStrategyBuilder::EXTRACT_CALLBACK => static function (DateTimeImmutable $data) {
                        return $data->format(DateTimeInterface::RFC3339_EXTENDED);
                    },
                ],
                'coreCreatedAt' => [
                    FormStrategyBuilder::DTO => DateTimeImmutable::class,
                    FormStrategyBuilder::HYDRATE_CALLBACK => static function ($date, $dto) {
                        /** @var \DateTimeImmutable $dto */
                        return $dto::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $date);
                    },
                    FormStrategyBuilder::EXTRACT_CALLBACK => static function (DateTimeImmutable $data) {
                        return $data->format(DateTimeInterface::RFC3339_EXTENDED);
                    },
                ],
                'orderStatusModelId' => [
                    FormStrategyBuilder::DTO => OrderStatusModelId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'id',
                ],
                'goods' => [
                    FormStrategyBuilder::DTO => Goods::class,
                    FormStrategyBuilder::IS_ARRAY => true
                ]
            ],
        ];
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray(self::getConfig($rootClassName));
    }
}
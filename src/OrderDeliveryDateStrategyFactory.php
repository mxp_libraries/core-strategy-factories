<?php
declare(strict_types=1);

namespace Maxipost\CoreStrategyFactories;

use Maxipost\CoreDomain\Common\ValueObject\Date;
use Maxipost\CoreDomain\Common\ValueObject\Time;
use Maxipost\CoreDomain\Common\ValueObject\TimeInterval;
use Maxipost\CoreDomain\Order\ValueObject\OrderId;
use Maxipost\FormStrategy\FormStrategyBuilder;
use Maxipost\FormStrategy\StrategyFactoryInterface;
use Zend\Hydrator\Strategy\StrategyInterface;

class OrderDeliveryDateStrategyFactory implements StrategyFactoryInterface
{
    /**
     * @var \Maxipost\FormStrategy\FormStrategyBuilder
     */
    private $formStrategyBuilder;

    public function __construct(FormStrategyBuilder $formStrategyBuilder)
    {
        $this->formStrategyBuilder = $formStrategyBuilder;
    }

    public static function getConfig(string $rootClassName): array
    {
        return [
            FormStrategyBuilder::DTO => $rootClassName,
            FormStrategyBuilder::NESTED_FIELDS => [
                '_id' => [
                    FormStrategyBuilder::DTO => OrderId::class,
                    FormStrategyBuilder::SINGLE_NAME => 'uuid'
                ],
                'date' => [
                    FormStrategyBuilder::DTO => Date::class,
                    FormStrategyBuilder::SINGLE_NAME => 'value',
                ],
                'timeInterval' => [
                    FormStrategyBuilder::DTO => TimeInterval::class,
                    FormStrategyBuilder::NESTED_FIELDS => [
                        'from' => [
                            FormStrategyBuilder::DTO => Time::class,
                            FormStrategyBuilder::SINGLE_NAME => 'value',
                        ],
                        'to' => [
                            FormStrategyBuilder::DTO => Time::class,
                            FormStrategyBuilder::SINGLE_NAME => 'value',
                        ],
                    ],
                ],
            ],
        ];
    }

    public function __invoke(string $rootClassName): StrategyInterface
    {
        return $this->formStrategyBuilder->buildFromArray(self::getConfig($rootClassName));
    }
}